#!/bin/bash

echo "digraph G {" > /tmp/graph.dot
find ./ -name "*.h" -exec sed -n 's/^\([[:blank:]]*\)class\([[:blank:]]\+\)\([^[:blank:]]\+\2\)\?\([^[:blank:]]\+\)\1\(:\1\(.*\)\)\?$\?/\4|\5/p' {} \; | grep -v ";" | while read DEC;
do
   CLASS=`echo $DEC | cut -d"|" -f1`
   echo "Processing class $CLASS ..."
   PARENTS=`echo $DEC | cut -d"|" -f2 | sed 's/^[[:blank:]]*:[[:blank:]]*//' | sed 's/ *, */,/'`
   IFS=$','
   for PARENT in $PARENTS
   do
      BASECLASS=`echo $PARENT | cut -d" " -f2`
      echo "$CLASS -> $BASECLASS" >> /tmp/graph.dot
   done 
   unset IFS
done
echo "}" >> /tmp/graph.dot
FILENAME=`pwd`
echo
echo "Generating graph ... please wait ..."
dot -Tpng -Kcirco /tmp/graph.dot > `basename $FILENAME`.png
echo
echo "Your graph was saved in `basename $FILENAME`.png"
echo
rm -rf /tmp/graph.dot
